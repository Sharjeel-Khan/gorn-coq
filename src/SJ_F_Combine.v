Require FinishLang.
Require FinishDF.
Require SafeJoins.

Require Import Tid.
Require Import Fid.
Require Import Var.
Require Import SafeJoins.
Require Import Coq.Lists.List.

Module F := FinishLang.
Module SJ := SafeJoins.

Notation finish_t := FinishDF.t.
Notation f_state := FinishDF.state.

Section Defs.
  Inductive ReducesN: list (tid*tid) -> SJ.trace -> Prop :=
  | reduces_n_nil:
    ReducesN nil nil
  | reduces_n_cons_fork:
    forall k t k' o,
    ReducesN k t ->
    CheckOp k o k' ->
    ReducesN k' (o :: t).

  Structure t := make {
    state : list (tid*tid);
    history : SJ.trace;
    spec: ReducesN state history
  }.

  (** A typed reduction *)

  Inductive Reduces: t -> op -> t -> Prop :=
  | reduces_def:
    forall o pm1 pm2,
    SJ.CheckOp (state pm1) o (state pm2) ->
    history pm2 = o::(history pm1) ->
    Reduces pm1 o pm2.
End Defs.

Notation sj_t := t.
Notation sj_state := state.

Module Semantics.

Inductive op :=
  | INIT: fid -> tid -> op
  | BEGIN_ASYNC: tid -> tid -> op
  | END_ASYNC: op
  | BEGIN_FINISH: fid -> op
  | END_FINISH: op
  | JOIN: tid -> tid -> op.

Inductive op_kind :=
  | finish_op
  | task_op
  | sj_op.

Definition get_op_kind (o:op) :=
  match o with
  | INIT _ _
  | BEGIN_ASYNC _ _
  | END_ASYNC => task_op
  | BEGIN_FINISH _
  | END_FINISH => finish_op
  | JOIN _ _ => sj_op
  end.

Inductive packet :=
  | only_sj: SJ.op -> packet
  | only_f: F.op -> packet
  | both: SJ.op -> F.op -> packet.

Definition translate (o:op) : packet :=
  match o with
  | INIT f t => only_f (F.INIT f)
  | BEGIN_ASYNC s d => both (SJ.SJ_Notations.F s d) (F.BEGIN_TASK d)
  | END_ASYNC => only_f (F.END_TASK)
  (* Pushes a finish scope  *)
  | BEGIN_FINISH f => only_f (F.BEGIN_FINISH f)
  (* Pops a finish scope *)
  | END_FINISH => only_f (F.END_FINISH)
  | JOIN s d => only_sj (SJ.SJ_Notations.J s d)
  end.

Definition creates_finish (o:op) :=
  match o with
  | INIT f t => Some f
  | BEGIN_FINISH f => Some f
  | _ => None
  end.

Definition as_f_op (o:op) :=
  match translate o with
  | only_f o => Some o
  | both _ o => Some o
  | only_sj _ => None
  end.

Ltac translate_solver := intros i; intros; destruct i; try (simpl in *; inversion H); compute; auto.

Lemma translate_only_f_impl_as_f_op:
    forall i o,
    translate i = only_f o ->
    as_f_op i = Some o.
  Proof.
    translate_solver.
  Qed.

Lemma translate_both_impl_as_f_op:
    forall i o o',
    translate i = both o o' ->
    as_f_op i = Some o'.
  Proof.
    translate_solver.
  Qed.

Lemma translate_only_sj_impl_as_f_op:
    forall i o,
    translate i = only_sj o ->
    as_f_op i = None.
  Proof.
    translate_solver.
  Qed.

Lemma as_f_op_some_impl_translate:
    forall i o,
    as_f_op i = Some o ->
    (translate i = only_f o) \/ (exists o', translate i = both o' o).
  Proof.
    intros.
    unfold as_f_op in H.
    remember (translate _).
    destruct p; try (inversion H; subst; intuition).
    right.
    exists o0.
    trivial.
  Qed.

Definition as_sj_op (o:op) :=
  match translate o with
  | only_sj o => Some o
  | both o _ => Some o
  | only_f _ => None
  end.

  Lemma translate_only_sj_impl_as_sj_op:
    forall i o,
    translate i = only_sj o ->
    as_sj_op i = Some o.
  Proof.
    translate_solver.
  Qed.

  Lemma translate_both_impl_as_sj_op:
    forall i o o',
    translate i = both o o' ->
    as_sj_op i = Some o.
  Proof.
    translate_solver.
  Qed.

  Lemma translate_only_f_impl_as_sj_op:
    forall i o,
    translate i = only_f o ->
    as_sj_op i = None.
  Proof.
    translate_solver.
  Qed.

  Lemma as_sj_op_some_impl_translate:
    forall i o,
    as_sj_op i = Some o ->
    (translate i = only_sj o) \/ (exists o', translate i = both o o').
  Proof.
    intros.
    unfold as_sj_op in H.
    remember (translate _).
    destruct p; try (inversion H; subst; intuition).
    right.
    exists o1.
    trivial.
  Qed.

Definition context := (sj_t * finish_t) % type.


End Semantics.

Module Typesystem.
  Import Semantics.

  Inductive CtxReduces (ctx:context) (t:tid): op -> Prop :=
  | valid_only_f:
    forall i o,
    translate i = only_f o ->
    F.Valid (f_state (snd ctx)) t o ->
    CtxReduces ctx t i
  | valid_only_sj:
    forall i o,
    translate i = only_sj o ->
    SJ.CanCheckOp (sj_state (fst ctx)) o ->
    CtxReduces ctx t i
  | valid_both:
    forall i o o',
    translate i = both o o' ->
    F.Valid (f_state (snd ctx)) t o' ->
    SJ.CanCheckOp (sj_state (fst ctx)) o ->
    CtxReduces ctx t i.

  Lemma valid_inv_f_check:
    forall sj f t i o,
    CtxReduces (sj, f) t i ->
    as_f_op i = Some o ->
    F.Valid (f_state f) t o.
  Proof.
    intros.
    inversion H; (subst; simpl in *).
    - apply translate_only_f_impl_as_f_op in H1.
      rewrite H1 in H0.
      inversion H0.
      subst.
      assumption.
    - apply translate_only_sj_impl_as_f_op in H1.
      rewrite H1 in H0.
      inversion H0.
    - apply translate_both_impl_as_f_op in H1.
      rewrite H1 in H0.
      inversion H0.
      subst.
      assumption.
  Qed.

  Lemma valid_inv_sj_check:
    forall sj f t i o,
    CtxReduces (sj, f) t i ->
    as_sj_op i = Some o ->
    SJ.CanCheckOp (sj_state sj) o.
  Proof.
    intros.
    inversion H; (subst; simpl in *).
    - apply translate_only_f_impl_as_sj_op in H1.
      rewrite H1 in H0.
      inversion H0.
    - apply translate_only_sj_impl_as_sj_op in H1.
      rewrite H1 in H0.
      inversion H0.
      subst.
      assumption.
    - apply translate_both_impl_as_sj_op in H1.
      rewrite H1 in H0.
      inversion H0.
      subst.
      assumption.
  Qed.

End Typesystem.
