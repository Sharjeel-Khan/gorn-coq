Require Coq.Lists.List.
Require FinishLang.
Require FinishDF.
Require Lang.
Require Progress.

Require Import Tid.
Require Import Cid.
Require Import Mid.
Require Import Fid.
Require Import Var.
Require Import SafeJoins.

Module Fi := FinishLang.
Module Fu := Lang.

Module Semantics.

Inductive op :=
  | INIT: fid -> op
  | BEGIN_ASYNC: tid -> op
  | END_ASYNC: op
  | BEGIN_FINISH: fid -> op
  | END_FINISH: op
  | READ: mid -> op
  | WRITE: mid -> op
  | BEGIN_FUTURE: tid -> op
  | END_FUTURE: op
  | FORCE: tid -> op
  | TAU: op.

Inductive op_kind :=
  | future_op
  | finish_op
  | task_op.

Definition get_op_kind (o:op) :=
  match o with
  | INIT _
  | BEGIN_ASYNC _
  | END_ASYNC => task_op
  | BEGIN_FINISH _
  | END_FINISH => finish_op
  | READ _
  | WRITE _
  | BEGIN_FUTURE _
  | END_FUTURE
  | FORCE _
  | TAU => future_op
  end.

Inductive packet :=
  | only_fu: Fu.op -> packet
  | only_fi: Fi.op -> packet
  | both: Fu.op -> Fi.op -> packet.

Definition translate (o:op) : packet :=
  match o with
  | INIT f => only_fi (Fi.INIT f)
  | BEGIN_ASYNC t => only_fi (Fi.BEGIN_TASK t)
  | END_ASYNC => only_fi (Fi.END_TASK)
  (* Pushes a finish scope  *)
  | BEGIN_FINISH f => only_fi (Fi.BEGIN_FINISH f)
  (* Drops all phasers and pops a finish scope *)
  | END_FINISH => only_fi (Fi.END_FINISH)
  | READ m => only_fu (Fu.READ m)
  | WRITE m => only_fu (Fu.WRITE m)
  | BEGIN_FUTURE t => both (Fu.FUTURE t) (Fi.BEGIN_TASK t)
  | END_FUTURE => only_fi (Fi.END_TASK)
  | FORCE t => only_fu (Fu.FORCE t)
  | TAU => only_fu (Fu.TAU)
  end.

Definition creates_finish (o:op) :=
  match o with
  | INIT f => Some f
  | BEGIN_FINISH f => Some f
  | _ => None
  end.

Definition as_fi_op (o:op) :=
  match translate o with
  | only_fi o => Some o
  | both _ o => Some o
  | only_fu _ => None
  end.

Ltac translate_solver := intros i; intros; destruct i; try (simpl in *; inversion H); compute; auto.

Lemma translate_only_fi_impl_as_fi_op:
    forall i o,
    translate i = only_fi o ->
    as_fi_op i = Some o.
  Proof.
    translate_solver.
  Qed.

Lemma translate_both_impl_as_f_op:
    forall i o o',
    translate i = both o o' ->
    as_fi_op i = Some o'.
  Proof.
    translate_solver.
  Qed.

Lemma translate_only_fu_impl_as_fi_op:
    forall i o,
    translate i = only_fu o ->
    as_fi_op i = None.
  Proof.
    translate_solver.
  Qed.

Lemma as_fi_op_some_impl_translate:
    forall i o,
    as_fi_op i = Some o ->
    (translate i = only_fi o) \/ (exists o', translate i = both o' o).
  Proof.
    intros.
    unfold as_fi_op in H.
    remember (translate _).
    destruct p; try (inversion H; subst; intuition).
    right.
    exists o0.
    trivial.
  Qed.

Definition as_fu_op (o:op) :=
  match translate o with
  | only_fu o => Some o
  | both o _ => Some o
  | only_fi _ => None
  end.

  Lemma translate_only_fu_impl_as_fu_op:
    forall i o,
    translate i = only_fu o ->
    as_fu_op i = Some o.
  Proof.
    translate_solver.
  Qed.

  Lemma translate_both_impl_as_fu_op:
    forall i o o',
    translate i = both o o' ->
    as_fu_op i = Some o.
  Proof.
    translate_solver.
  Qed.

  Lemma translate_only_fi_impl_as_fu_op:
    forall i o,
    translate i = only_fi o ->
    as_fu_op i = None.
  Proof.
    translate_solver.
  Qed.

  Lemma as_fu_op_some_impl_translate:
    forall i o,
    as_fu_op i = Some o ->
    (translate i = only_fu o) \/ (exists o', translate i = both o o').
  Proof.
    intros.
    unfold as_fu_op in H.
    remember (translate _).
    destruct p; try (inversion H; subst; intuition).
    right.
    exists o1.
    trivial.
  Qed.

Definition context := (future_t * finish_t) % type.


End Semantics.
